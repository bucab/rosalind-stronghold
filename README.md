# Intro

This repo contains an implementation of the exercises from the [Rosalind
Bioinformatics Stronghold](http://rosalind.info/problems/list-view/)
that can be run from the command line.

I created this repo for use in BF527 at BU and as BU Bioinformatics Hub
training materials. When I tried using Rosalind, I was frustrated by the
way so little feedback was given on submitting a solution that was incorrect.
This implementation is in pure python and uses pytest to compare the output
of the user's script and the "correct" output so users can know what they are
doing "wrong" immediately. This makes going through the exercises a more
effective learning experience, IMO.

# Install

To install, either use the bubhub conda channel:

`conda install -c bubhub rosalind-stronghold`

or simply install it using `python setup.py install` in the cloned repo
directory.

# Run

Once installed, the command `rosalind` should be available on your command
line. Running `rosalind list` will print out all the available exercises.

To get instructions on a particular exercise, run:

`rosalind info <exercise name>`.

This will print out instructions to stderr and scaffold python code to stdout,
enabling commands like `rosalind info <exercise name> > my_exercise.py`.

To test out a user submission for an exercise, run:

`rosalind submit <exercise name> <python script filename>`

A diff of the expected output and script output will then be printed to the
screen, confirming whether the script is correct.
