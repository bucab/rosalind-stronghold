'''
Usage:
  rosalind help
  rosalind list
  rosalind info <exercise_ID>
  rosalind run <exercise_ID>
  rosalind submit <exercise_ID> <python_filename>
'''
from __future__ import print_function
from collections import OrderedDict
from difflib import unified_diff
from docopt import docopt
import importlib
from io import TextIOWrapper, BytesIO
from jinja2 import Environment, PackageLoader, Template
import pkgutil
import runpy
import sys
import terminaltables
import traceback

from . import exercises as exercises_mod
from .util import stderr, stdout

class UserSubmissionError(Exception) : pass
class UnknownExerciseIDException(Exception) : pass
class IncorrectOutputError(Exception) : pass
class NoRunFunctionError(Exception) : pass
class BadExerciseDefinitionError(Exception) : pass

env = Environment(
        loader=PackageLoader('rosalind_stronghold', 'templates')
      )


class Exercise(object) :
  exercise_fields = ('name','ID','order','generate','run','template')
  def __init__(self,mod) :
    if any(_ not in dir(mod) for _ in Exercise.exercise_fields) :
      raise BadExerciseDefinitionError(
          'Exercise module {} does not define required fields: {}'
          .format(mod.__name__,Exercise.exercise_fields)
      )
    self.name = mod.name
    self.ID = mod.ID
    self.order = mod.order
    self._mod = mod

  def generate(self) :
    return self._mod.generate()
  def run(self,*args) :
    return self._mod.run(*args)
  def template(self) :
    return Template(self._mod.template()).render()
  def short_description(self) :
    return (env
         .get_template('short_description.md')
         .render(exercise=self)
       )
  def description(self) :
    return Template(self._mod.__doc__).render()
  def check(self,test_out,expected_out) :
    return hasattr(self._mod,'check') and self._mod.check(test_out,expected_out)

# need this to support python 2 and 3
# http://stackoverflow.com/questions/1218933/can-i-redirect-the-stdout-in-python-into-some-sort-of-string-buffer/19345047#19345047
class StdoutBuffer(TextIOWrapper):
  def write(self,string) :
    try:
      return super(StdoutBuffer,self).write(string)
    except TypeError:
      return super(StdoutBuffer, self).buffer.write(string)

def stdout_wrap(f,args) :

  # switch stdout to a StringIO object so we capture the
  # print stream of the run function
  old_stdout = sys.stdout
  sys.stdout = StdoutBuffer(BytesIO(), sys.stdout.encoding)

  f(*args)

  sys.stdout.seek(0)
  output = sys.stdout.read()

  # close the StringIO object to free up memory?
  sys.stdout.close()

  # put the stdout back where it used to be
  sys.stdout = old_stdout

  return output

def rosalind(argv=None) :

  opts = docopt(__doc__,argv)

  # load all the exercises
  exercises = []
  for importer, modname, ispkg in pkgutil.iter_modules(exercises_mod.__path__):
    exer_mod = importlib.import_module('.'+modname,'rosalind_stronghold.exercises')
    exercise = Exercise(exer_mod)
    exercises.append(exercise)

  exercises = sorted(exercises,key=lambda e: e.order)
  exercises = OrderedDict((_.ID,_) for _ in exercises)

  if opts['help'] :
    stdout(env.get_template('help.md').render())
    stdout(docopt(__doc__,['-h']))
  elif opts['list'] :
    exec_list = [['ID','Name','Suggested Order']]
    for name,exercise in exercises.items() :
      exec_list.append([name,exercise.name,exercise.order])
    table = terminaltables.SingleTable(exec_list,'Rosalind Exercises')
    stdout(table.table)
  else :

    exercise = exercises.get(opts['<exercise_ID>'])

    # check to make sure the passed ID is valid
    if exercise is None :
      raise NoRunFunctionError(
        env.get_template('no_exercise_id_found.md').render(
          ID=opts['<exercise_ID>']
        )
      )

    if opts['info'] :
      stderr(exercise.description())
      stdout(env.get_template('template_code.md').render(exercise=exercise))
    elif opts['run'] :

      example = exercise.generate()

      # run the correct code
      expected_output = stdout_wrap(exercise.run,example)

      stdout(
          env.get_template('run_output.md')
          .render(
            example=','.join([str(_) for _ in example])
            ,output=expected_output
          )
      )
    elif opts['submit'] :
      # construct an example
      example = exercise.generate()

      # run the user's code
      user_script = opts['<python_filename>']
      with open(user_script) as f :
        code = f.read()
        user_code = {}
        try :
          exec(code, user_code)

          if 'run' not in user_code :
            raise NoRunFunctionError('No run function found in {}, make sure it is defined'
              .format(opts['<python_filename>'])
            )

          user_output = stdout_wrap(user_code['run'],example)
        except NoRunFunctionError as e :
          raise e
        except Exception as e:
          stderr('User submitted code encountered an error:')
          traceback.print_exc()
          raise UserSubmissionError(*e.args)

      # run the correct code
      expected_output = stdout_wrap(exercise.run,example)

      # check if exercise has a test function
      # exercise.check returns true only if there is a check function
      # and that check function returns true
      if exercise.check(user_output,expected_output) :

        stdout(
          env.get_template('correct_output.md')
          .render(
            example=','.join([str(_) for _ in example])
            ,expected_output=expected_output
            ,user_output=user_output
          )
        )

      else:

        difflines = unified_diff(
            [_.strip() for _ in user_output.split('\n')]
            ,[_.strip() for _ in expected_output.split('\n')]
            ,'your output'
            ,'expected output'
        )
        difflines = '\n'.join(difflines)

        if len(difflines) != 0 :
          message = 'Your output differs from the expected output'

          stdout(
              env.get_template('diff_output.md')
              .render(message=message,diff=difflines,example=','.join([str(_) for _ in example])))
          raise IncorrectOutputError('')

        else :

          stdout(
            env.get_template('correct_output.md')
            .render(
              example=','.join([str(_) for _ in example])
              ,expected_output=expected_output
              ,user_output=user_output
            )
          )

def main() :
  try :
    rosalind(sys.argv[1:])
  except UserSubmissionError as e :
    stderr(e.args[0])
    sys.exit(1)
  except UnknownExerciseIDException as e :
    stderr(e.args[0])
    sys.exit(2)
  except IncorrectOutputError as e :
    stderr(e.args[0])
    sys.exit(3)
  except NoRunFunctionError as e :
    stderr(e.args[0])
    sys.exit(4)

if __name__ == '__main__' :
  main()
