from __future__ import print_function
import random
import sys

def random_dna_sequence(minlen=10,maxlen=1000) :
  seq_len = random.randint(minlen,maxlen)
  seq = ['ACGT'[random.randint(0,3)] for _ in range(seq_len)]
  return ''.join(seq)

def stderr(*args,**kwargs) :
  print(*args,file=sys.stderr,**kwargs)

def stdout(*args,**kwargs) :
  print(*args,file=sys.stdout,**kwargs)
