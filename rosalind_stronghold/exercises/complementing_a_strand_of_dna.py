'''
[Rosalind URL](http://rosalind.info/problems/revc/)

Problem

In DNA strings, symbols 'A' and 'T' are complements of each other, as are 'C'
and 'G'.

The reverse complement of a DNA string ss is the string scsc formed by reversing
the symbols of ss, then taking the complement of each symbol (e.g., the reverse
complement of "GTCA" is "TGAC").

*Given*: A DNA string ss of length at most 1000 bp.

*Return*: The reverse complement scsc of ss.
'''
name = 'Complementing a Strand of DNA'
ID = 'REVC'
order = 2

from ..util import random_dna_sequence

def generate() :
  return (random_dna_sequence(),)

# python 2 and 3 differ in how they implement maketrans
import sys
if sys.version_info[0] == 2 :
  import string
  compl = string.maketrans('ACTG','TGAC')
else :
  compl = str.maketrans('ACTG','TGAC')

def run(ss) :
  print(ss.translate(compl)[::-1])

tmpl_str = '''\
def run(ss) :
  # compute and print with the print() function the reverse complement of the
  #DNA sequence ss
  pass
'''
def template() :
  return tmpl_str
