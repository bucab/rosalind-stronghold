'''

[Rosalind URL](http://rosalind.info/problems/dna/)

# Problem

A string is simply an ordered collection of symbols selected from some alphabet
and formed into a word; the length of a string is the number of symbols that it
contains.

An example of a length 21 DNA string (whose alphabet contains the symbols 'A',
'C', 'G', and 'T') is "ATGCTTCAGAAAGGTCTTACG."

*Given*: A DNA string ss of length at most 1000 nt.

*Return*: Four integers (separated by spaces) counting the respective number of
times that the symbols 'A', 'C', 'G', and 'T' occur in ss.

# Sample Dataset

AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC

# Sample Output

20 12 17 21

'''
from __future__ import print_function
from __future__ import absolute_import
from ..util import random_dna_sequence

name = 'Counting DNA Nucleotides'
ID = 'DNA'
order = 0

def generate() :
  return (random_dna_sequence(),)

def run(s) :
  for c in list('ACGT') :
    print(s.count(c),end=' ')
  print() # newline

tmpl_str = '''\
def run(s) :
  # use the print() function to print out counts of A, C, G, and T in s on one
  # line separated by spaces
  pass
'''
def template() :
  return tmpl_str
