# Rosalind Stronghold Bioinformatics Exercises

This is a command line implementation of the bioinformatics programming
problems in [Rosalind Stronghold](http://rosalind.info). To get started, type:

$ rosalind list

This will print out a list of the currently available puzzles. Note the ID of one
of the puzzles. The remainder of this help assumes you are working with the
puzzle ID "DNA", but the commands work for any ID from the `rosalind list` table.
For more information on the DNA puzzle, run:

$ rosalind info DNA

This will print out a description of the puzzle and a python function template
you can use to get started. To save the template to a file, type:

$ rosalind info DNA > DNA_problem.py

You can then add code to the file `DNA_problem.py` to attempt to solve the
puzzle. To see example input and expected output of a given puzzle, type:

$ rosalind run DNA

When you think your code is ready to compare with the expected output, you can
submit it for comparison with the following, e.g.:

$ rosalind submit DNA DNA_problem.py

The output will show you if your output matches what is expected, and confirms
when you are correct.

