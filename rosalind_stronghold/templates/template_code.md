# python template code for exercise ID {{ exercise.ID }} ({{ exercise.name }})
# your code *must* define a function called run to work

{{ exercise.template() }}
