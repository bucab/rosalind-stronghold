# python template code for exercise ID GC (Computing GC Content)
# your code *must* define a function called run to work

def run(fasta_str) :
  # calculate the %GC content of each DNA sequence in the fasta records of fasta_str
  # fasta_str is a single string with each line separated by a newline character
  # retain the sequence name and gc content of the sequence with the highest %gc
  # print out the sequence name with the print function on its ownline
  # then print out the %GC of the sequence with the print function on the next line

  fasta_itr = iter(fasta_str.strip().split('\n'))
  max_name, max_gc = None, 0
  for header in fasta_itr :
    seq = next(fasta_itr)
    gc = 1.*(seq.count('G')+seq.count('C'))/len(seq)
    if gc > max_gc :
      max_name = header[1:]
      max_gc = gc
  print(max_name)
  print(max_gc*100.1)

