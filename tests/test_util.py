from rosalind_stronghold import util
import pytest
import random

#Test that random_dna_sequence returns a sequence with length between 10 and 1000
def test_random_dna_sequence_random():
        seq=util.random_dna_sequence()
        length=len(seq)
        assert 10<=length<=1000

#Test that random_dna_sequence returns a sequence with specified length when the same number is input for each parameter
def test_random_dna_sequence_specifylen():
        length=random.randint(10,1000)
        seq=util.random_dna_sequence(length,length)
        assert len(seq)==length