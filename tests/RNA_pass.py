# python template code for exercise ID RNA (Transcribing DNA into RNA)
# your code *must* define a function called run to work

def run(t) :
  # print out the DNA sequence in t replacing all Ts with Us
  print(t.replace('T','U'))
