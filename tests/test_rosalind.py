import pytest
from glob import glob
import os

from rosalind_stronghold import rosalind
from rosalind_stronghold.rosalind import (
    UserSubmissionError
    ,UnknownExerciseIDException
    ,IncorrectOutputError
    ,NoRunFunctionError
    ,BadExerciseDefinitionError
    )

def test_syntax_error() :
  with pytest.raises(UserSubmissionError) :
    rosalind(['submit','DNA',os.path.join('tests','python_syntax_error.py')])

def test_no_run_error() :
  with pytest.raises(NoRunFunctionError) :
    rosalind(['submit','DNA',os.path.join('tests','no_run_function.py')])

# create the tests for pass and fail programmatically based on the
# existence of ID_pass.py and ID_fail.py
def make_pass_func(fn) :
  ID = fn.split('/')[-1].split('_')[0]
  def f() :
    assert rosalind(['submit',ID,fn]) is None
  return f

def make_fail_func(fn) :
  ID = fn.split('/')[-1].split('_')[0]
  def f() :
    with pytest.raises(IncorrectOutputError) :
       rosalind(['submit',ID,fn])
  return f

# py.test must be invoked from the root repo dir for this to work!
test_dir = 'tests' 
for fn in glob(os.path.join(test_dir,'*_pass.py')) :
  basename = os.path.splitext(fn)[0]
  locals()['test_{}'.format(basename)] = make_pass_func(fn)

for fn in glob(os.path.join(test_dir,'*_fail.py')) :
  basename = os.path.splitext(fn)[0]
  locals()['test_{}'.format(basename)] = make_fail_func(fn)
