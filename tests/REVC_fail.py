def run(ss) :
  for c in ss[::-1] :
    if c == 'A' : print('A',end='')
    if c == 'C' : print('G',end='')
    if c == 'G' : print('C',end='')
    if c == 'T' : print('A',end='')
  print()
